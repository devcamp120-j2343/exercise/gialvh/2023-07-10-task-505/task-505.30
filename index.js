//import thư viện expressjs
const express = require("express");

//khởi tạo 1 app express
const app = express();

//khai báo cổng
const port = 8000;

//khai báo để app đọc được body json
app.use(express.json());

//request params - áp dụng cho GET,PUT,DELETE
//request params là những tham số xuất hiện trong ỦL của API
app.get("/request-params/:param1/:param2/param", (req, res) => {
    let param1 = req.params.param1;
    let param2 = req.params.param2;

    res.json({
        params: {
            param1,
            param2
        }
    })
})

//request Query - chỉ áp dụng cho phương thức GET 
// khi lấy request query bắt buộc phải validate
app.get("/request-query", (req, res) => {
    let query = req.query;

    res.json({
        query
    })
})

//request Body JSON - chỉ áp dụng cho phương thức POST, PUT
//khi lấy request body phải validate
//cần khai báo thêm ở dòng 11
app.post("/request-body-json", (req, res) => {
    let body = req.body;

    res.json({
        body
    })
})

app.listen(port, () => {
    console.log("App listening on port: ", port);
})